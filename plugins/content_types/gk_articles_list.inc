<?php

$plugin = array(
  'title' => 'Articles: List',
  'category' => 'GK Articles',
  'single' => TRUE,
);

function gk_articles_gk_articles_list_content_type_render($subtype, $conf, $args, $context) {
  // Build an array of options to pass to gk_articles_get_items().
  $options = array(
    'filters' => array(
      'promote' => $conf['promote'],
      'sticky' => $conf['sticky'],
    ),
    'paged' => $conf['paged'],
    'per_page' => $conf['per_page'],
    'exclude_current_node' => $conf['exclude_current_node'],
  );

  if ($conf['limit']) {
    $options['limit'] = $conf['limit'];
  }

  if ($articles = gk_articles_get_items($options)) {
    $content = array();

    foreach ($articles as $article) {
      ObjectMetaDataStorage::set($article, $conf);
      $content[$article->nid] = node_view($article, $conf['view_mode']);
    }

    if ($options['paged']) {
      $content['pager'] = array(
        '#theme' => 'pager',
      );
    }

    return (object) array(
      'title' => $conf['override_title'] ? $conf['override_title_text'] : '',
      'content' => $content,
    );
  }
}

function gk_articles_gk_articles_list_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  // Compile a list of view modes for the view mode select form
  $entity_info = entity_get_info('node');
  $view_modes = array('default' => t('Default'));

  foreach ($entity_info['view modes'] as $key => $item) {
    $view_modes[$key] = $item['label'];
  }

  $form['view_mode'] = array(
    '#type' => 'select',
    '#title' => t('View Mode'),
    '#options' => $view_modes,
    '#default_value' => isset($conf['view_mode']) ? $conf['view_mode'] : 'teaser',
  );

  $form['promote'] = array(
    '#type' => 'select',
    '#title' => 'Promoted',
    '#options' => array(
      1 => 'Yes',
      0 => 'No',
    ),
    '#empty_option' => 'Both',
    '#empty_value' => 'both',
    '#default_value' => isset($conf['promote']) ? $conf['promote'] : NULL,
  );

  $form['sticky'] = array(
    '#type' => 'select',
    '#title' => 'Stickied',
    '#options' => array(
      1 => 'Yes',
      0 => 'No',
    ),
    '#empty_option' => 'Both',
    '#empty_value' => 'both',
    '#default_value' => isset($conf['sticky']) ? $conf['sticky'] : NULL,
  );

  $form['paged'] = array(
    '#type' => 'checkbox',
    '#title' => 'Paged',
    '#default_value' => isset($conf['paged']) ? $conf['paged'] : 0,
  );

  $form['per_page'] = array(
    '#type' => 'textfield',
    '#title' => 'Per page',
    '#default_value' => isset($conf['per_page']) ? $conf['per_page'] : 5,
    '#element_validate' => array('element_validate_number'),
    '#states' => array(
      'visible' => array(
        ':input[name="paged"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['limit'] = array(
    '#type' => 'textfield',
    '#title' => 'Limit',
    '#default_value' => isset($conf['limit']) ? $conf['limit'] : 0,
    '#element_validate' => array('element_validate_number'),
  );

  $form['exclude_current_node'] = array(
    '#type' => 'checkbox',
    '#title' => 'Exclude current node',
    '#default_value' => isset($conf['exclude_current_node']) ? $conf['exclude_current_node'] : 1,
  );

  return $form;
}

function gk_articles_gk_articles_list_content_type_edit_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $form_state['conf'] = $form_state['values'];

  if ($form_state['conf']['promote'] == 'both') {
    $form_state['conf']['promote'] = NULL;
  }

  if ($form_state['conf']['sticky'] == 'both') {
    $form_state['conf']['sticky'] = NULL;
  }
}
