<?php

$plugin = array(
  'title' => 'Articles: Categories',
  'category' => 'GK Articles',
  'single' => TRUE,
);

function gk_articles_gk_articles_categories_content_type_render($subtype, $conf, $args, $context) {
  $query = db_select('taxonomy_index', 'ti')
    ->distinct()
    ->fields('ti', array('tid'))
    ->condition('tv.machine_name', 'article_categories')
    ->addTag('node_access');

  $query->join('taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
  $query->join('taxonomy_vocabulary', 'tv', 'ttd.vid = tv.vid');

  if ($tids = $query->execute()->fetchCol()) {
    if ($terms = taxonomy_term_load_multiple($tids)) {
      $items = array();

      foreach ($terms as $term) {
        $items[] = array(
          'data' => l($term->name, 'taxonomy/term/' . $term->tid),
          'class' => array(
            drupal_html_class($term->name),
          ),
        );
      }

      if (!empty($items)) {
        return (object) array(
          'title' => $conf['override_title'] ? $conf['override_title_text'] : 'Categories',
          'content' => array(
            '#theme' => 'item_list',
            '#items' => $items,
          ),
        );
      }
    }
  }
}

function gk_articles_gk_articles_categories_content_type_edit_form($form, &$form_state) {
  return $form;
}
