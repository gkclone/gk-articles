<?php
/**
 * @file
 * gk_articles.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function gk_articles_user_default_roles() {
  $roles = array();

  // Exported role: article creator.
  $roles['article creator'] = array(
    'name' => 'article creator',
    'weight' => 2,
  );

  // Exported role: article editor.
  $roles['article editor'] = array(
    'name' => 'article editor',
    'weight' => 3,
  );

  return $roles;
}
