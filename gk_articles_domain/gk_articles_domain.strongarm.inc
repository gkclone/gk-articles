<?php
/**
 * @file
 * gk_articles_domain.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function gk_articles_domain_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'domain_node_article';
  $strongarm->value = array(
    0 => 'DOMAIN_ACTIVE',
  );
  $export['domain_node_article'] = $strongarm;

  return $export;
}
