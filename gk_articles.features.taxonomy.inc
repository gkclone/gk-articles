<?php
/**
 * @file
 * gk_articles.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function gk_articles_taxonomy_default_vocabularies() {
  return array(
    'article_categories' => array(
      'name' => 'Article categories',
      'machine_name' => 'article_categories',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
